#ifndef LANTERNCONTROLPROTOCOL_H
#define LANTERNCONTROLPROTOCOL_H

#include <QWidget>
#include <QTcpSocket>

class LanternWidget;
class TLV;
class LanternControlProtocol : public QWidget
{
    Q_OBJECT
public:
    LanternControlProtocol(const QString& strHost = "127.0.0.1", int nPort = 9999, QWidget* pwgt = 0) ;

protected:
    void closeEvent( QCloseEvent *);

private slots:
    void slotReadyRead();
    void slotError(QAbstractSocket::SocketError socketError);

private:
    void setCommand(const TLV& tlv);

private:
    QTcpSocket*         m_socket;
    LanternWidget*      m_lantern = nullptr;
};

#endif // LANTERNCONTROLPROTOCOL_H
