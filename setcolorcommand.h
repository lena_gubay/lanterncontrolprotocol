#ifndef SETCOLORCOMMAND_H
#define SETCOLORCOMMAND_H

#include "basecommand.h"

class SetColorCommand : public BaseCommand
{
public:
    SetColorCommand(LanternWidget* object, QByteArray value): BaseCommand(object) , m_value(value) {}
    void execute() override
    {
        quint8 red;
        quint8 green;
        quint8 blue;
        QDataStream s(m_value);
        s >> red;
        s >> green;
        s >> blue;
        m_object->setColor(QColor(red, green, blue));
    }

private:
    QByteArray m_value;
};

#endif // SETCOLORCOMMAND_H
