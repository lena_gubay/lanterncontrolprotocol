#include "tlv.h"

#include <QDataStream>

bool TLV::isValid()
{
    bool ret = true;
    if (m_length != m_value.length())
        ret = false;
    return ret;
}

QDataStream& operator>>( QDataStream& d, TLV& tlv )
{
    char* command = new char[1];
    d >> command;
    tlv.m_type = command[0];

    d >> tlv.m_length;
    if (tlv.m_length > 0) {
        QByteArray byteArray( tlv.m_length, '\0' );
        d.readRawData( byteArray.data(), tlv.m_length );
        tlv.m_value = byteArray;
    }

    return d;
}
