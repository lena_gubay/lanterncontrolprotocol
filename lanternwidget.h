#ifndef LANTERNWIDGET_H
#define LANTERNWIDGET_H

#include <QWidget>

class LanternWidget : public QWidget
{
    Q_OBJECT
public:
    LanternWidget(const QColor &color, QWidget *parent = nullptr);

    bool isOn() const { return m_on; }
    void setOn(bool on);

public slots:
    void turnOff() { setOn(false); }
    void turnOn() { setOn(true); }
    void setColor(const QColor& color);

protected:
    void resizeEvent(QResizeEvent *event) override;
    void paintEvent(QPaintEvent * event) override;

private:
    void drawLed(const QColor &color);

private:
    bool m_on;

    int m_height;
    int m_width;
    int minDim;
    int m_half;
    int m_centerX;
    int m_centerY;
    QRect m_drawingRect;

    int m_outerBorderWidth;
    int m_innerBorderWidth;
    int m_outerBorderRadius;
    int m_innerBorderRadius;
    int m_topReflexY;
    int m_bottomReflexY;
    int m_topReflexWidth;
    int m_topReflexHeight;
    int m_bottomReflexWidth;
    int m_bottomReflexHeight;

    QColor m_currentLedColor;
    QColor m_lightColor;
    QColor m_shadowColor;
    QColor m_ringShadowDarkColor;
    QColor m_ringShadowMedColor;
    QColor m_ringShadowLightColor;
    QColor m_topReflexUpColor;
    QColor m_topReflexDownColor;
    QColor m_bottomReflexCenterColor;
    QColor m_bottomReflexSideColor;

};

#endif // LANTERNWIDGET_H
