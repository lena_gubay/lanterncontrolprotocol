#include "lanterncontrolprotocol.h"
#include "turnoncommand.h"
#include "turnoffcommand.h"
#include "setcolorcommand.h"
#include "lanternwidget.h"
#include "tlv.h"

#include <QVBoxLayout>
#include <QMessageBox>
#include <QAbstractSocket>

LanternControlProtocol::LanternControlProtocol(const QString& strHost,
                   int            nPort,
                   QWidget*       pwgt /*=0*/
                  ) : QWidget(pwgt)
{
    m_socket = new QTcpSocket(this);
    connect(m_socket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error), this, &LanternControlProtocol::slotError);
    connect(m_socket, &QTcpSocket::readyRead, this, &LanternControlProtocol::slotReadyRead);

    m_socket->connectToHost(strHost, nPort);

    QVBoxLayout *vbox = new QVBoxLayout(this);
    m_lantern = new LanternWidget(Qt::red);
    vbox->addWidget(m_lantern);
}


void LanternControlProtocol::slotReadyRead()
{
    QByteArray block = m_socket->readAll();

    QDataStream in(&block, QIODevice::ReadOnly);
    in.setVersion(QDataStream::Qt_5_13);

    while (!in.atEnd()) {
        TLV tlv;
        in >> tlv;
        if (tlv.isValid())
            setCommand(tlv);
    }
}

void LanternControlProtocol::slotError(QAbstractSocket::SocketError socketError)
{
    QString strError =
        "Error: " + (socketError == QAbstractSocket::HostNotFoundError ?
                     "The host was not found." :
                     socketError == QAbstractSocket::RemoteHostClosedError ?
                     "The remote host is closed." :
                     socketError == QAbstractSocket::ConnectionRefusedError ?
                     "The connection was refused." :
                     QString(m_socket->errorString())
                    );

    QMessageBox msgBox;
    msgBox.setText(strError);
    msgBox.exec();
}

void LanternControlProtocol::setCommand(const TLV& tlv)
{
    if (tlv.m_type == 0x12 && (tlv.m_length == 0)) {
        TurnOnCommand command(m_lantern);
        command.execute();
    }
    else if (tlv.m_type == 0x13 && (tlv.m_length == 0)) {
        TurnOffCommand command(m_lantern);
        command.execute();
    }
    else if((tlv.m_type == 0x20) && (tlv.m_length ==3)) {
        SetColorCommand command(m_lantern, tlv.m_value);
        command.execute();
    }
}

void LanternControlProtocol::closeEvent(QCloseEvent *event)
{
    m_socket->disconnectFromHost();
    m_socket->close();
    m_socket->deleteLater();
    QWidget::closeEvent(event);
}
