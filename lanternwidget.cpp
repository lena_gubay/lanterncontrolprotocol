
#include "lanternwidget.h"
#include <QPainter>

LanternWidget::LanternWidget(const QColor &color, QWidget *parent) : QWidget(parent),
     m_on(false),
    m_currentLedColor(color),

    m_lightColor(QColor(0xE0, 0xE0, 0xE0)),
    m_shadowColor(QColor(0x70, 0x70, 0x70)),
    m_ringShadowDarkColor(QColor(0x50, 0x50, 0x50, 0xFF)),
    m_ringShadowMedColor(QColor(0x50, 0x50, 0x50, 0x20)),
    m_ringShadowLightColor(QColor(0xEE, 0xEE, 0xEE, 0x00)),
    m_topReflexUpColor(QColor(0xFF, 0xFF, 0xFF, 0xA0)),
    m_topReflexDownColor(QColor(0xFF, 0xFF, 0xFF, 0x00)),
    m_bottomReflexCenterColor(QColor(0xFF, 0xFF, 0xFF, 0x00)),
    m_bottomReflexSideColor(QColor(0xFF, 0xFF, 0xFF, 0x70))
{}

void LanternWidget::setColor(const QColor &color)
{
    m_currentLedColor = color;
    update();
}

void LanternWidget::paintEvent(QPaintEvent * event)
{
    if (m_on)
        this->drawLed(m_currentLedColor);
    else
        this->drawLed(Qt::gray);
    QWidget::paintEvent(event);
}

void LanternWidget::setOn(bool on)
{
    if (on == m_on)
        return;
    m_on = on;
    update();
}


void LanternWidget::drawLed(const QColor &color)
{
    QPainter p(this);

    QPen pen;
    pen.setStyle(Qt::NoPen);
    p.setPen(pen);

    QRadialGradient outerRingGradient(QPoint(m_centerX,
                              m_centerY - m_outerBorderRadius - (m_outerBorderWidth / 2)),
                              minDim - (m_outerBorderWidth / 2));
    outerRingGradient.setColorAt(0, m_lightColor);
    outerRingGradient.setColorAt(1, m_shadowColor);
    QBrush outerRingBrush(outerRingGradient);
    p.setBrush(outerRingBrush);
    p.drawEllipse(this->m_drawingRect);

    QRadialGradient innerRingGradient(QPoint(m_centerX,
                              m_centerY + m_innerBorderRadius  + (m_innerBorderWidth / 2)),
                              minDim - (m_innerBorderWidth / 2));
    innerRingGradient.setColorAt(0, m_lightColor);
    innerRingGradient.setColorAt(1, m_shadowColor);
    QBrush innerRingBrush(innerRingGradient);
    p.setBrush(innerRingBrush);
    p.drawEllipse(QPoint(m_centerX, m_centerY),
                  m_outerBorderRadius, m_outerBorderRadius);

    QColor dark(color.darker(120));
    QRadialGradient glassGradient(QPoint(m_centerX, m_centerY),
                              m_innerBorderRadius );
    glassGradient.setColorAt(0, color);
    glassGradient.setColorAt(1, dark);
    QBrush glassBrush(glassGradient);
    p.setBrush(glassBrush);
    p.drawEllipse(QPoint(m_centerX, m_centerY),
                  m_innerBorderRadius,
                  m_innerBorderRadius);

    QRadialGradient shadowGradient(QPoint(m_centerX, m_centerY),
                                  m_innerBorderRadius );
    shadowGradient.setColorAt(0, m_ringShadowLightColor);
    shadowGradient.setColorAt(0.85, m_ringShadowMedColor);
    shadowGradient.setColorAt(1, m_ringShadowDarkColor);
    QBrush shadowBrush(shadowGradient);
    p.setBrush(shadowBrush);
    p.drawEllipse(QPoint(m_centerX, m_centerY),
                  m_innerBorderRadius,
                  m_innerBorderRadius);

    QLinearGradient topTeflexGradient(QPoint(m_centerX,
                                         (m_innerBorderWidth + m_outerBorderWidth)),
                                  QPoint(m_centerX, m_centerY));
    topTeflexGradient.setColorAt(0, m_topReflexUpColor);
    topTeflexGradient.setColorAt(1, m_topReflexDownColor);
    QBrush topReflexbrush(topTeflexGradient);
    p.setBrush(topReflexbrush);
    p.drawEllipse(QPoint(m_centerX, m_topReflexY), m_topReflexWidth, m_topReflexHeight);

    QRadialGradient bottomReflexGradient(QPoint(m_centerX,
                                     m_bottomReflexY + (m_bottomReflexHeight / 2)),
                              m_bottomReflexWidth);
    bottomReflexGradient.setColorAt(0, m_bottomReflexSideColor);
    bottomReflexGradient.setColorAt(1, m_bottomReflexCenterColor);
    QBrush bottomReflexBrush(bottomReflexGradient);
    p.setBrush(bottomReflexBrush);
    p.drawEllipse(QPoint(m_centerX, m_bottomReflexY),
                  m_bottomReflexWidth,
                  m_bottomReflexHeight);

}

void LanternWidget::resizeEvent(QResizeEvent *event)
{
    QWidget::resizeEvent(event);
    this->m_height = this->size().height();
    this->m_width = this->size().width();
    this->minDim = (m_height > m_width) ? m_width : m_height;
    this->m_half = minDim / 2;
    this->m_centerX = m_width / 2;
    this->m_centerY = m_height / 2;

    this->m_outerBorderWidth = minDim / 10;
    this->m_innerBorderWidth = minDim / 14;
    this->m_outerBorderRadius = m_half - m_outerBorderWidth;
    this->m_innerBorderRadius = m_half - (m_outerBorderWidth + m_innerBorderWidth);

    this->m_topReflexY = m_centerY
            - (m_half - m_outerBorderWidth - m_innerBorderWidth) / 2;
    this->m_bottomReflexY = m_centerY
            + (m_half - m_outerBorderWidth - m_innerBorderWidth) / 2;
    this->m_topReflexHeight = m_half / 5;
    this->m_topReflexWidth = m_half / 3;
    this->m_bottomReflexHeight = m_half / 5;
    this->m_bottomReflexWidth = m_half / 3;

    m_drawingRect.setTop((m_height - minDim) / 2);
    m_drawingRect.setLeft((m_width - minDim) / 2);
    m_drawingRect.setHeight(minDim);
    m_drawingRect.setWidth(minDim);
}
