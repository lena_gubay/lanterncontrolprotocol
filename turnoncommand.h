#ifndef TURNONCOMMAND_H
#define TURNONCOMMAND_H

#include "basecommand.h"

class TurnOnCommand : public BaseCommand
{
    Q_OBJECT
public:
    TurnOnCommand(LanternWidget* object): BaseCommand(object){}
    void execute() override
    {
        m_object->turnOn();
    }
};

#endif // TURNONCOMMAND_H
