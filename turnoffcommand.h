#ifndef TURNOFFCOMMAND_H
#define TURNOFFCOMMAND_H

#include "basecommand.h"


class TurnOffCommand : public BaseCommand
{
public:
    TurnOffCommand(LanternWidget* object): BaseCommand(object){}
    void execute() override
    {     m_object->turnOff(); }
};

#endif // TURNOFFCOMMAND_H
