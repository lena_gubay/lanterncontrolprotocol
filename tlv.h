#ifndef TLV_H
#define TLV_H

#include <QObject>
// TLV data structure
class TLV
{
public:
    char m_type;
    qint16 m_length;
    QByteArray m_value;

    bool isValid();
    friend QDataStream& operator>>( QDataStream& d, TLV& tvl );
};

QDataStream& operator>>( QDataStream& d, TLV& tlv );

#endif // TLV_H
