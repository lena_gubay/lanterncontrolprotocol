#ifndef BASECOMMAND_H
#define BASECOMMAND_H

#include <QObject>
#include "lanternwidget.h"

class BaseCommand : public QObject
{
    Q_OBJECT
public:
    explicit BaseCommand(LanternWidget* object) : m_object(object) {}
    virtual void execute() = 0;

protected:
    LanternWidget* m_object;
};

#endif // BASECOMMAND_H
