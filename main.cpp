#include "lanterncontrolprotocol.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    LanternControlProtocol w("localhost", 9999);
    w.resize(300, 300);
    w.show();

    return a.exec();
}
